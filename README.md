configuration
=========

Reads the configuration of different boxes (company, team, my, etc.).

Requirements
------------

None.

Role Variables
--------------

| Variable                       | Required | Description                                                               | Default                 |
|--------------------------------|----------|---------------------------------------------------------------------------|-------------------------|
| ibox_user_configuration_file   | true     | Location of the configuration file. All other variables will be read from | ~/.config/ibox/ibox.yml |
| ibox_projects_directory        | false    | Location your projects.                                                   | ''                      |
| ibox_configuration_directories | true     | List of configuration directories.                                        | []                      |
| ibox_box_configuration_files   | true     | List of files in configuration directories.                               | []                      |

Dependencies
------------

None.

Example Playbook
----------------

Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.configuration.git
    version: stable
    scm: git
    name: ibox.configuration
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.configuration"
      ansible.builtin.import_role:
        name: "ibox.configuration"
```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
